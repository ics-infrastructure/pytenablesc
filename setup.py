#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open("README.md") as f:
        return f.read()


setup(
    name="pytenablesc",
    version="0.0.1",
    description="pytenablesc is a cli tool to retrieve data from Tenable.sc using its REST API.",
    long_description=readme(),
    url="https://gitlab.esss.lu.se/ics-infrastructure/pytenablesc",
    author="Remy Mudingay",
    author_email="remy.mudingay@ess.eu",
    license="MIT",
    scripts=["bin/pytenablesc"],
    install_requires=["argparse", "requests", "termcolor"],
    test_suite="tests",
    include_package_data=True,
    zip_safe=False,
)
