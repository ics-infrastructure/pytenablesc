#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# API reference https://docs.tenable.com/tenablesc/api/index.htm
# reference https://developer.tenable.com/reference

import os
import sys
import json
import collections


from tenable.sc import TenableSC

try:
    import requests
    import argparse
    from termcolor import colored
except Exception:
    print("Please install the dependencies first")
    print("You can install them by typing pip install -r requirements.txt\n")
    exit()


usage = "\n\tpytenablesc [OPTIONS] -f format -t type\n"
example = """For Example:

       pytenablesc -f json -t vuln
       pytenablesc -f csv -t vuln -s 4
       pytenablesc -f list -t asset
       pytenablesc -f json -t assetlist

"""
parser = argparse.ArgumentParser(
    usage=usage, epilog=example, formatter_class=argparse.RawDescriptionHelpFormatter
)
 

group = parser.add_argument_group()


group.add_argument(
    "-f",
    "--format",
    dest="format",
    type=str,
    help="Specify format: json, csv or list",
    choices=['csv', 'json', 'list'],
)


group.add_argument(
    "-t",
    "--type",
    dest="type",
    type=str,
    help="Data type to return: asset, assetlist, vuln",
    required=True,
    choices=['asset', 'assetlist', 'vuln'],
)


parser.add_argument(
    "-s",
    "--severity",
    dest="severity",
    type=int,
    help="static Severity: 0 [info], 1 [low], 2 [medium], 3 [high] or 4 [critical]",
    required=False,
    choices=range(0, 4),
)


args = parser.parse_args()

# Prints help if no argument is passed
if not len(sys.argv) > 1 or "--help" in sys.argv or "-h" in sys.argv:
    parser.print_help()
    exit()


# set  variable when -f flag is used
if args.format and (args.format == 'csv' or args.format == 'json' or args.format == 'list'):
    format = args.format


# set  variable when -t flag is used
if args.type and (args.type == 'asset' or args.type == 'assetlist' or args.type == 'vuln'):
    type = args.type


# set  variable when -s flag is used
if args.severity and args.severity in range(0, 4):
    severity = args.severity
else:
    severity = 4


# username and password for loging in
nessus_user = os.environ.get("NESSUS_USER")
nessus_pass = os.environ.get("NESSUS_PASS")
username = nessus_user
password = nessus_pass


# Alter the url filled if Nessus is running on a remote machine
nessus_host = os.environ.get("NESSUS_HOST")
url = nessus_host


# Display 
print("Print list of vulnerable hosts with severity level of at least 4 (Critical)")
print("===========================================================================")
sc = TenableSC(nessus_host)
sc.login(nessus_user, nessus_pass)


def list_assetList():
   list_array = []
   assetlist = sc.asset_lists.list(['id', 'name'])
   for asset_list in assetlist['usable']:
       list_array.append(asset_list)
   return list_array


def list_assets():
   list_array = []
   assetlist = sc.asset_lists.details(185)
   return assetlist


def list_vulns():
   list_array = []
   for vuln in sc.analysis.vulns(('severity', '=', severity), ('exploitAvailable', '=', 'true')):
       t = ('{dnsName}'.format(**vuln), '{ip}'.format(**vuln), '{pluginID}'.format(**vuln), '{pluginName}'.format(**vuln))
       list_array.append(t)
   return list_array


def r_asset():
   print(list_assets())


def r_assetList():
   print(list_assetList())


def r_vulns():
   objects_list = []
   for row in list_vulns():
       d = collections.OrderedDict()
       d['dnsName'] = row[0]
       d['ip'] = row[1]
       d['pluginInfo'] = row[2]
       d['pluginName'] = row[3]
       objects_list.append(d)
   if format == 'json':
       print(json.dumps(objects_list))
   elif format == 'csv':
       print(json.dumps(objects_list))
   elif format == 'list':
       print(json.dumps(objects_list))


def main():
    # call functions from here
    if type == 'asset':
        r_asset()
    elif type == 'assetlist':
        r_assetList()
    elif type == 'vuln':
        r_vulns()


if __name__ == "__main__":
   main()
   #print(rJson())
