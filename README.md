[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![pipeline](https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify/badges/master/pipeline.svg)](https://gitlab.esss.lu.se/ics-infrastructure/pytenablesc/pipelines)
[![coverage](https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify/badges/master/coverage.svg)](https://gitlab.esss.lu.se/ics-infrastructure/pytenablesc/pipelines)

## Python-Tenablesc
pytenablesc is a **command line utility** written in *python*, which provides the user facility to *retrieve scan results from Tenable.sc using its REST API*.

Depending on the flag issued, one can *retrieve data*, in different formats*, in order to include vulnerability, inventory and patch management in your infrastructure as code lifecyle. The command line utility has the *power* to do all this just by entering a single command.

## Pre-Requisites
```
python3.x
Tenable.sc
pip for python3.x
```
## Installation
Ideally, you should be able to just type:
```bash
git clone https://gitlab.esss.lu.se/ics-infrastructure/pytenablesc
cd pytenablesc
pip3 install . # doing this in a virtual environment is better
```
## Preparations
*pytenablesc* requires a username, password and Tenable.sc's https address.
These should be set as environment variables as shown below.

Set the following environment variables as follows:

>`NESSUS_USER=readonly`
>`NESSUS_PASS=readonlapassy`
>`NESSUS_HOST=https://tenablesc.domain`

## How to use
To obtain a list of all vulnerabilities, with a default severity of 4 (Critical):

>`pytenablesc -f json -t vuln`

To obtain a list of all vulnerabilities, with a default severity of 3 (High):
>`pytenablesc -f csv -t vuln -s 3`

To obtain a list of asset types and thier ids:
>`pytenablesc -f json -t assetlist`

To obtain a list of assets:
>`pytenablesc -f json -t asset`

## Export Report formats
```
_________________________________________________________________________
| csv    | comma-separated values (CSV)                                  |
| json   | JavaScript Object Notation format.                            |
| list   | unformatted list                                              |
|________________________________________________________________________|
```
## How to uninstall
```bash
pip3 uninstall pytenablesc
```
## API Reference
https://docs.tenable.com/tenablesc/api/index.htm
